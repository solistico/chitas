  var app=angular
              .module('chitas', ['ngAnimate','ui.router'])
        
			  
			
app.config(function($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise("/main");
  
  
  
  $stateProvider
  
	
	   .state('main', {
      url: "/main",
      templateUrl: "src/view/main.html",
      controller: function($rootScope,$scope,rambam3,rambam1,chumash,dateService) {
		    var self = $scope;


$rootScope.isInMenu = true;
$scope.$on('$destroy', function () {
	$rootScope.isInMenu = false;
});


self.bArray = [ {
		text : "חומש",
		url : "#/reader/chumash"
	}, {
		text : "תהילים",
		url : "#/reader/tehilim"
	}, {
		text : "תניא",
		url : "#/reader/tanya"
	}, {
		text : "היום יום",
		url : "#/reader/hy"
	},{
		text : "רמבם פרק 1",
		url : "#reader/rambam1"
	}, {
		text : "רמבם 3 פרקים",
		url : "#/reader/rambam3"
	}, {
		text : "ספר המצוות",
		url : "#/reader/sm"
	}
]
			
			 
		 }
		  
      }
    )
	
	
    .state('reader', {
      url: "/reader",
      templateUrl: "src/view/reader.html"
    })
	
	.state('reader.hy', {
    	url : "/hy",
    	templateUrl : "src/view/readers/hy.html",
    	controller : function ( $scope, getHyService , hy ,dateService ) {
    		
			
			
			
    			var self = $scope;
    			self.date = dateService.getDateObj();
    			$scope.$watch("date", function () {
    				var hyChap  = hy();
					
					getHyService(hyChap[0][0],hyChap[0][1]).then(function (data) {
						self.text = data
    				})

					

    			},true)

    	}
    })
	.state('reader.tehilim', {
    	url : "/tehilim",
    	templateUrl : "src/view/readers/tehilim.html",
    	controller : function ( $scope, tehilim ,dateService ) {
    		
			
			
			
    			var self = $scope;
    			self.date = dateService.getDateObj();
    			$scope.$watch("date", function () {
    				var tehilimChap  = tehilim();
					self.title = tehilimChap['c'];
					self.pages=[];
					tehilimChap['c'];
					for (i=tehilimChap['s'];i<=tehilimChap['e'];i++) {
						
						
						self.pages.push(i);
						
					}
					
					

    			},true)

    	}
    })
	
	
	
	.state('reader.sm', {
    	url : "/sm",
    	templateUrl : "src/view/readers/sm.html",
    	controller : function ($interval,$timeout, $scope,storedVars, sm , getSmService,dateService ) {
    		
			
			
				
    			var self = $scope;
    			self.date = dateService.getDateObj();
    			
				var pageName="sm";
				

				
				
				
				$scope.$watch("date", function () {
    				var rambamChap = sm();
					self.title = rambamChap['text'];
					
					// u know'that clusure thingie...
					function populate(pointer,number,chapter) {
						
						getSmService(chapter).then(function (data) {
								pointer[number] = data
						})

					}
					
					self.texts=[];
					
					for (var i=0; i<rambamChap['lessonValue'].length; i++) {
						
						populate(self.texts,i,rambamChap['lessonValue'][i])
							
					}
					
					

    			},true)

    	}
    })
	
	
	.state('reader.rambam3', {
    	url : "/rambam3",
    	templateUrl : "src/view/readers/rambam3.html",
    	controller : function ($interval,$timeout, $scope,storedVars, rambam3 , getRambamService,dateService ) {
    		
			
			
				
    			var self = $scope;
    			self.date = dateService.getDateObj();
    			
				var pageName="rambam3";
				if (storedVars.getVar(pageName + "_date") == self.date.uxTime) {
						
						var scrollTo=storedVars.getVar(pageName + "_position");
						
						$timeout(function(){
							$("body").scrollTop( scrollTo );  
							
						},1000)
						
						
					}
					
				var cancel = $interval(function(){
						storedVars.setVar(pageName+"_date",self.date.uxTime);
						storedVars.setVar(pageName+"_position",$("body").scrollTop());
				},500);
				
				$scope.$on('$destroy', function () {						
					$interval.cancel(cancel);
				});
				
				
				
				$scope.$watch("date", function () {
    				var rambamChap = rambam3();
					self.title = rambamChap['text'];
    				getRambamService(rambamChap['lessonValue']).then(function (data) {
						self.text1 = data
    				})

    				getRambamService(rambamChap['lessonValue']+1).then(function (data) {
						self.text2 = data
    				})

    				getRambamService(rambamChap['lessonValue']+2).then(function (data) {
						self.text3 = data
    				})
					
					

    			},true)

    	}
    })
	
	
	 .state('reader.rambam1', {
    	url : "/rambam1",
    	templateUrl : "src/view/readers/rambam1.html",
    	controller : function ($scope,$interval,$timeout,storedVars, rambam1, getRambamService,dateService ) {
    		
			
			
			
    			var self = $scope;
    			self.date = dateService.getDateObj();
				
				
								var pageName="rambam1";
				if (storedVars.getVar(pageName + "_date") == self.date.uxTime) {
						
						var scrollTo=storedVars.getVar(pageName + "_position");
						
						$timeout(function(){
							$("body").scrollTop( scrollTo );  
							
						},1000)
						
						
					}
					
				var cancel = $interval(function(){
						storedVars.setVar(pageName+"_date",self.date.uxTime);
						storedVars.setVar(pageName+"_position",$("body").scrollTop());
				},500);
				
				$scope.$on('$destroy', function () {						
					$interval.cancel(cancel);
				});

				
				
    			$scope.$watch("date", function () {
    				var rambamChap = rambam1();
					self.title = rambamChap['text'];
    				getRambamService(rambamChap['lessonValue']).then(function (data) {

    					
						self.text = data;

    				})

    			},true)

    	}
    })
	
    .state('reader.tanya', {
    	url : "/tanya",
    	templateUrl : "src/view/readers/tanya.html",
    	controller : function ($scope, tanya ,dateService ) {
    		
			
			
			
    			var self = $scope;
    			self.date = dateService.getDateObj();
    			$scope.$watch("date", function () {
    				var tanyaChap = tanya();
    				self.title = tanyaChap['c'];
    				self.pages = [];
    				
    				for (i = tanyaChap['s']; i <= tanyaChap['e']; i++) {

    					self.pages.push(i);

    				}
					

					


					},true)

    	}
    })
    .state('reader.chumash', {
    	url : "/chumash",
    	templateUrl : "src/view/readers/chumash.html",
    	controller : function ( $scope, storedVars,chumash, getChumashService,dateService ) {
    		
				
    			var self = $scope;
				
				self.chul=storedVars.getVar("chul")||false;
    			self.date = dateService.getDateObj();
    			self.$watchGroup(["date.uxTime","chul"], function () {
    				var chumashChap = chumash(self.chul);
    				getChumashService(chumashChap['pID'], chumashChap['pDay']).then(function (data) {
						storedVars.setVar("chul",self.chul); 
    					self.title = "פרשת " + chumashChap['pName'] + ", " + chumashChap['pAlia']
    						self.text = data;

    				})

    			},true)

    	}
    })
});




(function(){

app.controller('topNavCntl', [
            "dateService" ,
          topNavCntl
       ]);

  
function topNavCntl(dateService) {
    var self = this;
    this.shOpen=false;
	this.date=dateService.getDateObj();
    this.nextDay=function(){dateService.nextDay()}
    this.prevDay=function(){dateService.prevDay()}

	
	 
      this.goMenu = function() {
          window.location.hash="#/main"
		  
		
		  
      };




  }

})();


