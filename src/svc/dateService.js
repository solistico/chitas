(function () {
	'use strict';

	app
	.service('dateService', ["numToHebLetter",dateService]);

	function dateService(numToHebLetter) {

		/*  	----          ----                -------- */
		var calculateDateObj = function () {

			currentDate.dateObj = dateObj;
			var hebDate =  new Hebcal.HDate(dateObj)
			currentDate.hebDate = hebDate;
			currentDate.dateText = hebDay(dateObj.getDay()+1)  + " " + numToHebLetter(hebDate.getDate()) + " " + hebMonth(hebDate.getMonthName())  + " " + numToHebLetter(hebDate.getFullYear())   
			currentDate.uxTime = Math.floor( dateObj.getTime() / (1000*60*60*24))  * 60*60*24;
			currentDate.yearMode=ymode(hebDate.getFullYear())
			
		}
		
		function hebDay (aNumber) {
					
					if (aNumber==1) return "ראשון";
					if (aNumber==2) return "שני";
					if (aNumber==3) return "שלישי";
					if (aNumber==4) return "רביעי";
					if (aNumber==5) return "חמישי";
					if (aNumber==6) return "שישי";
					if (aNumber==7) return "שבת";
		}
		
		function hebMonth(monthName) {
					if (monthName == 'Tishrei') {		return "תשרי"    }
					if (monthName == 'Cheshvan'){		return "חשוון"    }
					if (monthName == 'Kislev')	{		return "כסלו"    }
					if (monthName == 'Tevet')	{		return "טבת"    }
					if (monthName == 'Sh\'vat')	{		return "שבט"    }
					if (monthName == 'Adar')	{		return "אדר"    }
					if (monthName == 'Adar 1')	{		return "אדר א'"    }
					if (monthName == 'Adar 2')	{		return "אדר ב'"    }
					if (monthName == 'Nisan')	{		return "ניסן"    }
					if (monthName == 'Iyyar')	{		return "אייר"    }
					if (monthName == 'Sivan')	{		return "סיוון"    }
					if (monthName == 'Tamuz')	{		return "תמוז"    }
					if (monthName == 'Av')		{		return "אב"        }
					if (monthName == 'Elul')	{		return "אלול"    }
					return false;
				}
		
		
		var ymode = function (hebYear) {
			var year_mode = 'k';			
			var playDate = new Hebcal.HDate();
			playDate.setFullYear(hebYear);
			playDate.setMonth(7);
			playDate.setDate(1);
			var roshDay = playDate.getDay() + 1;
			year_mode = year_mode + roshDay;
			playDate.setMonth(8);
			var cheshvanDays = playDate.getMonthObject().days.length;
			playDate.setMonth(9);
			var KislevDays = playDate.getMonthObject().days.length;

			if (cheshvanDays == 30) {

				year_mode = year_mode + 'a';

			} else if (KislevDays == 30) {

				year_mode = year_mode + 'r';

			} else {

				year_mode = year_mode + 'l';

			}

			playDate.setMonth(12);

			if (playDate.isLeapYear()) {

				year_mode = year_mode + 'm';

			} else {

				year_mode = year_mode + 'p';

			}

			return year_mode;

		}
		

		// CONSTRUCT
		var dateObj = new Date;
		dateObj.setTime(moment().startOf('day').valueOf())
		
		var currentDate = {
			
		}

		calculateDateObj();
		// END CONSTRUCT


		return {
			
			getDateObj : function () {
				return currentDate;
			},

			nextDay : function () {

				dateObj.setDate(dateObj.getDate() + 1);
				calculateDateObj();

			},
			prevDay : function () {

				dateObj.setDate(dateObj.getDate() - 1);
				calculateDateObj();

			}
		}
	};

})();
