(function () {
	'use strict';

	app
	.service('getHyService', ["$http", "$q","$sce", getHyService]);

	function getHyService($http, $q,$sce) {

		return function (month, day) {
			var ret = $q.defer();

			$http({
				method : 'get',
				url : 'data/hy/' + month + '-' + day + '.txt',
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
				}
			}).
			success(function (data, status, headers, config) {

				ret.resolve($sce.trustAsHtml(data));
			}).
			error(function (data, status, headers, config) {

				ret.reject(status);

			});
			return ret.promise;
		}

	}

})();


(function () {
	'use strict';

	app
	.service('getChumashService', ["$http", "$sce" ,"$q", getChumashService]);

	function getChumashService($http, $sce,$q) {

		return function (parsha, day) {
			var ret = $q.defer();

			$http({
				method : 'get',
				url : 'data/chumash/' + parsha + '/' + day + '.html',
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
				}
			}).
			success(function (data, status, headers, config) {

				ret.resolve($sce.trustAsHtml(data));
			}).
			error(function (data, status, headers, config) {

				ret.reject(status);

			});
			return ret.promise;
		}

	}

})();



(function () {
	'use strict';

	app
	.service('getRambamService', ["$http", "$sce" ,"$q", getRambamService]);

	function getRambamService($http, $sce, $q) {

		return function (lesson) {
			var ret = $q.defer();

			$http({
				method : 'get',
				url : 'data/rambam/' + lesson + '.txt',
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
				}
			}).
			success(function (data, status, headers, config) {
				
				data = data.replace( new RegExp('\r?\n','g') ,"<br/>");
				data = data.split("<br/>");
				
				var shifted='';
				do {
					shifted = data.shift();
				} while(shifted!='text') 

				do {
					shifted = data.shift();
				} while(shifted!='text') 
				
			
				data = data.join("<br/>");
				
				data = $sce.trustAsHtml(data);
				ret.resolve(data);
			}).
			error(function (data, status, headers, config) {

				ret.reject(status);

			});
			return ret.promise;
		}

	}

})();




  
	
(function () {
	'use strict';

	app
	.service('getSmService', ["$http", "$sce" ,"$q", getSmService]);

	function getSmService($http, $sce, $q) {

		return function (lesson) {
			var ret = $q.defer();

			$http({
				method : 'get',
				url : 'data/mitzvot/' + lesson + '.html',
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
				}
			}).
			success(function (data, status, headers, config) {
				
				data = $sce.trustAsHtml(data);
				
				ret.resolve(data);
			}).
			error(function (data, status, headers, config) {

				ret.reject(status);

			});
			return ret.promise;
		}

	}

})();




  
	
	
	
(function () {
	'use strict';

	app
	.service('storedVars', [function () {

		 
		
	  return {

        setVar: function (varName, varValue) {
            window.localStorage['storedVars_' + varName] = JSON.stringify(varValue);
        },

        getVar: function (varName) {
            if (typeof window.localStorage['storedVars_' + varName] != 'string') return undefined;
            else return JSON.parse(window.localStorage['storedVars_' + varName]);
        }

    }}]);

	
	

})();


