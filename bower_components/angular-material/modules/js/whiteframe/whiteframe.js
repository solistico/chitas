/*!
 * Angular Material Design
 * https://github.com/angular/material
 * @license MIT
 * v0.9.0-rc2-master-8fb60c3
 */
(function () {
"use strict";
/**
 * @ngdoc module
 * @name material.components.whiteframe
 */
angular.module('material.components.whiteframe', []);

})();