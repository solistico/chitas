/*!
 * Angular Material Design
 * https://github.com/angular/material
 * @license MIT
 * v0.9.0-rc2-master-8fb60c3
 */
goog.provide('ng.material.components.whiteframe');

/**
 * @ngdoc module
 * @name material.components.whiteframe
 */
angular.module('material.components.whiteframe', []);

ng.material.components.whiteframe = angular.module("material.components.whiteframe");