הלכות שמיטה ויובל

text

פרק ח

text

הלכה א: כדרך שאסור לעבוד הארץ בשביעית כך אסור לחזק ידי ישראל שעובדין אותה, או למכור להן כלי עבודה, לפי שאסור לחזק ידי עוברי עבירה.

הלכה ב: ואלו כלים שאין האומן רשאי למוכרן בשביעית למי שחשוד על השביעית, מחרישה וכל כליה העול והמזרה והדקר, זה הכלל כל שמלאכתו מיוחדת למלאכה שאסורה בשביעית אסור למכרו לחשוד, ולמלאכה שאפשר שתהיה אסורה ותהיה מותרת מותר למוכרו לחשוד.

הלכה ג: כיצד מוכר הוא לו המגל והעגלה וכל כליה, שאם יקצור בו מעט ויביא על העגלה מעט ה"ז מותר, ואם יקצור כדרך הקוצרין או יביא כל פירות שדהו אסור.

הלכה ד: ומותר למכור סתם למי שאינו חשוד אפילו דבר שמלאכתו מיוחדת למלאכה האסורה בשביעית, שהרי אפשר שקנה בשביעית לעשות לו מלאכה לאחר שביעית.

הלכה ה: היוצר מוכר חמש כדי שמן וחמשה עשר כדי יין, ומותר למכור לעכו"ם יתר מזה ואינו חושש שמא ימכור לישראל, ומוכר כדים רבים לישראל בחו"ל ואינו חושש שמא יביאם לארץ.

הלכה ו: ומוכר לחשוד פרה חורשת בשביעית שהרי אפשר לשוחטה, ומוכר לו שדהו שהרי אפשר שיובירה, אבל לא ימכור לו שדה האילן אלא אם כן פסק עמו על מנת שאין לו באילן, ומשאילו סאה למדוד בה אעפ"י שהוא יודע שיש לו גורן שהרי אפשר שימדוד בה בתוך ביתו, ופורט לו מעות אף ע"פ שהוא יודע שיש לו פועלים, וכולן בפירוש אסורים.

הלכה ז: וכן משאלת אשה לחברתה החשודה על השביעית נפה וכברה רחים ותנור, אבל לא תבור ולא תטחון עמה.

הלכה ח: מחזיקין ידי עכו"ם בשביעית בדברים בלבד, כגון שראהו חורש או זורע אומר לו תתחזק או תצליח וכיוצא בדברים אלו, מפני שאינם מצווין על שביתת הארץ אבל לא יסעדנו ביד, ומותר לרדות עמהן הכוורת וחוכרין מהן נירין לפי שאינן בני חיוב כדי לקנוס אותן.

הלכה ט: מותר לעשות בסוריא בתלוש אבל לא במחובר, כיצד דשין וזורין ודורכין ומעמרין, אבל לא קוצרין ולא בוצרין ולא מוסקין וכן כל כיוצא באלו.

הלכה י: כשם שאסור לעשות סחורה בפירות שביעית או לשמרן כך אסור ליקח מעם הארץ, לפי שאין מוסרין דמי שביעית לעם הארץ, ואפילו כל שהוא, שמא לא יאכל אותן בקדושת שביעית.

הלכה יא: הלוקח לולב מעם הארץ בשביעית נותן לו אתרוג מתנה, ואם לא נתן לו מבליע לו דמי אתרוג בדמי לולב.

הלכה יב: במה דברים אמורים בזמן שהיה מוכר פירות שכמותן בשמור כגון תאנים ורמונים וכיוצא בהן, אבל היה מוכר פירות שחזקתן מן ההפקר כגון הפיגם והירבוזין והשוטים והחלגלוגות והכסבר של הרים וכיוצא בהן ה"ז מותר ליקח ממנו מעט כדמי שלש סעודות בלבד משום כדי חייו של מוכר.

הלכה יג: וכל דבר שאינו חייב במעשרות כגון שום בעל בכי ובצל של רכפה וגריסין הקלקיות ועדשים המצריות וכן זרעוני גינה שאינן נאכלות כגון זרע לפת וצנון וכיוצא בהן הרי אלו נלקחין מכל אדם בשביעית.

הלכה יד: בד"א בעם הארץ סתם, אבל מי שהוא חשוד לעשות סחורה בפירות שביעית, או לשמור פירותיו ולמכור מהן אין לוקחין ממנו דבר שיש עליו זיקת שביעית כלל, ואין לוקחין ממנו פשתן אפי' סרוק, אבל לוקחין ממנו טווי ושזור.

הלכה טו: החשוד על השביעית אינו חשוד על המעשרות, והחשוד על המעשרות אינו חשוד על השביעית, שאע"פ שזה וזה מה"ת, מעשר טעון הבאת מקום מה שאין כן בשביעית, ושביעית אין לה פדיון משא"כ במעשר.

הלכה טז: החשוד על הטהרות אינו חשוד לא על המעשר ולא על השביעית, שהאוכל הטמא הזה שמכרו בחזקת טהור אינו מטמא אחרים אלא מדברי סופרים, והחשוד לדברי סופרים אינו חשוד לדברי תורה.

הלכה יז: כל החשוד על דבר אע"פ שאינו נאמן על של עצמו נאמן הוא על של אחרים חזקה אין אדם חוטא לאחרים, לפיכך החשוד על הדבר דנו ומעידו. 

הלכה יח: הכהנים חשודין על השביעית, לפי שהם אומרים הואיל והתרומות מותרות לנו אע"פ שהן אסורין על הזרים במיתה ק"ו פירות שביעית, לפיכך סאה תרומה שנפלה למאה סאה של פירות שביעית תעלה, נפלה לפחות ממאה ירקבו הכל ולא ימכרו לכהנים ככל מדומע לפי שהם חשודין על השביעית.

הלכה יט: הצבענין והפטמין לוקחין מורסן מכל מקום, ואינן חוששין שמא מספיחי שביעית הוא.

הלכה כ: גבאי קופה בשביעית לא יהיו מדקדקין בחצרות של אוכלין שביעית, ואם נתנו להן פת מותרת ואין חוששין לה שמא מספיחי שביעית הוא, שלא נחשדו ישראל להיות נותנין אלא או מעות שביעית או ביצים הנלקחות בדמי שביעית, ומותר ללות מן העניים פירות שביעית ומחזירין להן פירות בשנה שמינית.