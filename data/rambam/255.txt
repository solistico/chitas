הלכות נערה בתולה

text

פרק ג

text

הלכה א: המוציא שם רע על בת ישראל ונמצא הדבר שקר לוקה שנ' ויסרו אותו ואזהרה שלו מלא תלך רכיל בעמך, ונותן לאביה משקל מאה סלעים כסף מזוקק ואם היתה יתומה הרי הן של עצמה.

הלכה ב: והמוציא שם רע על הקטנה או על הבוגרת פטור מן הקנס ומן המלקות ואינו חייב עד שיוציא על הנערה שנ' והוציאו את בתולי הנערה נערה מלא דבר הכתוב.

הלכה ג: אין דנין דין זה אלא בפני הבית ובבית דין של עשרים ושלשה מפני שיש בדין מוציא שם רע דיני נפשות שאם נמצא הדבר כמו שאמר הרי זו נהרגת אבל האונס והמפתה דנין בהן בכל זמן בשלשה כמו שיתבאר בהלכות סנהדרין.

הלכה ד: ומצות עשה של תורה שתשב אשת מוציא שם רע תחתיו לעולם שנ' ולו תהיה לאשה אפילו עורת או מוכת שחין, ואם גירשה עבר על לא תעשה שנ' לא יוכל לשלחה כל ימיו וכופין אותו ומחזיר ואינו לוקה כמו שבארנו באונס, ואם קדם אחר וקדשה או שמתה או שהיה כהן שאסור בגרושה לוקה על גירושיה.

הלכה ה: נמצא בה דבר זמה או שנמצאת אסורה עליו מחייבי לאוין או מחייבי עשה ואפילו שנייה הרי זה יגרשנה בגט שנ' ולו תהיה לאשה אשה הראויה לו, ולמה לא יבא עשה וידחה את לא תעשה בין במוציא שם רע בין באונס וישא זו האסורה לו, שהרי אפשר שלא תרצה היא לישב ונמצא עשה ולא תעשה קיימין.

הלכה ו: כיצד הוצאת שם רע הוא שיבא לבית דין ויאמר נערה זו בעלתי ולא מצאתי לה בתולים וכשבקשתי על הדבר נודע לי שזינתה תחתי אחר שארסתיה ואלו הן עידי שזינתה בפניהן ובית דין שומעין דברי העדים וחוקרין עדותן אם נמצא הדבר אמת נסקלת, ואם הביא האב עדים והזימו העדים שהביא הבעל ונמצאו שהעידו שקר יסקלו וילקה הוא ויתן מאה סלע, ועל זה נאמר ואלה בתולי בתי אלו העדים שיזימו עידי הבעל, חזר הבעל והביא עדים אחרים והזים עידי האב הרי הנערה ועידי אביה נסקלין ועל זה נאמר ואם אמת היה הדבר הזה, מפי השמועה למדו שפרשה זו יש בה עדים וזוממין וזוממי זוממין.

הלכה ז: הוציא עליה שם רע והיא בוגרת אע"פ שהביא עדים שזינת תחתיו כשהיתה נערה הרי זה פטור מן המלקות ומן הקנס, ואם נמצא הדבר אמת הרי זו תסקל אע"פ שהיא בוגרת הואיל ובעת שזינתה נערה היתה.

הלכה ח: כל נערה שאין לה קנס אם נאנסה או נתפתתה כך המוציא עליה שם רע פטור מן המלקות ומן התשלומין, וכן הנכרית שנתגיירה והשפחה שנשתחררה פחותה מבת שלש שנים אפילו היתה הורתה שלא בקדושה ולידתה בקדושה המוציא עליה שם רע פטור מן הקנס ומן המלקות שנ' כי הוציא שם רע על בתולת ישראל עד שתהיה הורתה ולידתה בקדושה.

הלכה ט: קידש נערה וגירשה וחזר וקידשה והוציא עליה שם רע והביא עדים שזינתה תחתיו בקדושין הראשונים ונמצאו זוממים הרי זה פטור, וכן אם היתה יבמתו שכנסה והוציא עליה שם רע והביא עדים שזינתה תחת קידושי אחיו ונמצאו זוממים הרי זה פטור מן המלקות ומן התשלומין, וכל הפטור אם רצה לגרש יגרש.

הלכה י: אינו חייב עד שיבעול אותה כדרכה ויוציא שם רע כדרכה, בעלה שלא כדרכה ואמר לא מצאתיה בתולה פטור ומכין אותו מכת מרדות.

הלכה יא: וכן אם אמר לא מצאתיה בתולה ולא אמר שזינתה תחתי או שאמר זינתה תחתי ולא הביא עדים אלא באו מאליהם הרי זה פטור אע"פ שהעדים נהרגים אם הוזמו.

הלכה יב: זה שנ' בתורה ופרשו השמלה לשון כבוד שנושאין ונותנין בסתרי הדבר, וכן זה שיאמר האב ואלה בתולי בתי הן זוממי עידי הבעל, וזה שנ' ואם אמת היה הדבר תהרג בשזינת אחר האירוסין בעדים שנ' לזנות בית אביה, אבל קודם האירוסין כבר דנה תורה בה שהיא פטורה מכלום ובועלה חייב בתשלומי ממון בלבד בין פיתה בין אנס. בריך רחמנא דסייען.