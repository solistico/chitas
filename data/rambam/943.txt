הלכות סנהדרין

text

פרק ז

text

הלכה א: אחד מבעלי דינין שאמר איש פלוני ידון לי ואמר בעל דינו פלוני ידון לי הרי אלו שני הדיינים שבירר זה אחד וזה אחד הם בוררים להן דיין שלישי ושלשתן דנין לשניהן שמתוך כך יצא הדין לאמתו, אפילו היה האחד שביררו בעלי הדין חכם גדול וסמוך אינו יכול לכוף את בעל דינו שידון אצל זה אלא גם הוא בורר מי שירצה.

הלכה ב: מי שקבל עליו קרוב או פסול בין להיותו דיין בין להיותו עד עליו, אפילו קבל אחד מן הפסולים בעבירה כשני עדים כשרים להעיד עליו או כשלשה בית דין מומחין לדון לו, בין שקבל על עצמו לאבד זכיותיו ולמחול מה שהיה טוען על פיהן, בין שקבל שיתן כל מה שיטעון עליו חבירו בעדות זו הפסול או בדינו, אם קנו מידו על זה אינו יכול לחזור בו, ואם לא קנו מידו יכול לחזור בו עד שיגמר הדין, נגמר הדין והוציא הממון בדין זה הפסול או בעדותו אינו יכול לחזור.

הלכה ג: וכן מי שנתחייב לחבירו שבועה בבית דין ואמר לו השבע לי בחיי ראשך והפטר או השבע לי בחיי ראשך ואתן לך כל מה שתטעון, אם קנו מידו אינו יכול לחזור בו ואם לא קנו מידו יכול לחזור בו עד שיגמר הדין, נגמר הדין ונשבע כמו שאמר לו אינו יכול לחזור וחייב לשלם.

הלכה ד: והוא הדין למי שנתחייב שבועת היסת והפכה, אם קנו מידו או אם נשבע זה שנהפכה עליו אינו יכול לחזור בו.

הלכה ה: והוא הדין במי שלא היה חייב שבועה ואמר אשבע לך שבועה, אם קנו מידו אינו יכול לחזור בו, ואם לא קנו מידו אע"פ שקבל בבית דין חוזר עד שיגמר הדין וישבע.

הלכה ו: מי שנתחייב בבית דין והביא עדים או ראיה לזכותו, סותר את הדין וחוזר הדין, אע"פ שכבר נגמר הדין כל זמן שהוא מביא ראיה סותר, אמרו לו הדיינים כל ראיות שיש לך הבא מכאן ועד שלשים יום אף על פי שהביא ראיה לאחר שלשים יום סותר את הדין מה יעשה אם לא מצא בתוך שלשים ומצא לאחר שלשים.

הלכה ז: אבל אם סתם את טענותיו אינו סותר, כיצד אמרו לו יש לך עדים אמר אין לי עדים, יש לך ראיה אמר אין לי ראיה ודנו אותו וחייבוהו, כיון שראה שנתחייב אמר קרבו פלוני ופלוני והעידוני או שהוציא ראיה מתוך אפונדתו אין זה כלום ואין משגיחין על עדיו ועל ראיתו.

הלכה ח: במה דברים אמורים כשהיתה הראיה אצלו והעדים עמו במדינה, אבל אם אמר אין לי עדים ואין לי ראיה ולאחר מכאן באו לו עדים ממדינת הים, או שהיתה החמת של אביו שיש שם השטרות מופקדת ביד אחרים ובא זה שהפקדון אצלו והוציא לו ראיתו הרי זה מביא וסותר, ומפני מה סותר מפני שיכול לטעון ולומר זה שאמרתי אין לי עדים אין לי ראיה מפני שלא היו מצויין אצלי, וכל זמן שיכול לטעון ולומר זה שאמרתי אין לי עדים אין לי ראיה מפני שלא היו מצויין אצלי או מפני כך וכך אמרתי אין לי עדים ואין לי ראיה והיה ממש בדבריו הרי זה לא סתם טענותיו וסותר, לפיכך אם פירש ואמר אין לי עדים כלל לא הנה ולא במדינת הים ולא ראיה כלל לא בידי ולא ביד אחרים אינו יכול לסתור.

הלכה ט: במה דברים אמורים בגדול שנתחייב והביא ראיות והביא עדים אחר שסתם טענותיו, אבל יורש שהוא קטן כשמת מורישו ובאו עליו טענות מחמת מורישו אחר שהגדיל ואמר אין לי עדים ואין לי ראיה ואחר שיצא מבית דין חייב אמרו לו אחרים אנו יודעים לאביך עדות שתסתור בה דין זה, או אמר לו אחד מורישך הפקיד ראיה זו, הרי זה מביא מיד וסותר שאין היורש קטן יודע כל ראיות מורישו.  הלכה י: מי שקנו מידו שאם לא יבא ביום פלוני וישבע יהיה חבירו נאמן בטענתו ויטול כל מה שטען בלא שבועה, או שאם לא יבא ביום פלוני וישבע ויטול אבד את זכותו ואין לו כלום ויפטר חבירו, ועבר היום ולא בא נתקיימו התנאים ואבד את זכותו, ואם הביא ראיה שהיה אנוס באותו היום הרי זה פטור מקנין זה וישבע כשיתבענו חבירו כשהיה מקודם וכן כל כיוצא בזה.