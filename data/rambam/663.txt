הלכות שאר אבות הטומאה

text

פרק יד

text

הלכה א: שנים עשר ספיקות טהרו חכמים ואלו הן: ספק מים שאובים למקוה, ספק טומאה צפה על פני המים, ספק משקין לטמא אחרים, אבל לטומאת עצמן טמאים מספק, ספק הידים בין לטומאת עצמן בין לטמא אחרים בין לטהרות ידים מטומאתן, ספק דברי סופרים, ספק החולין, ספק קרבנות, ספק נגעים, ספק עובר ועומד, ספק שרצים, ספק ר"ה, ספק שתי רשויות.

הלכה ב: ספק מים שאובין למקוה כיצד, שלשה לוגין מים שאובין שנפלו למקוה פסלוהו, ספק נפלו ספק לא נפלו ואפילו נפלו ספק יש בהן שיעור או אין בהן ספיקו טהור והרי המקוה בכשרותו ואין מורין לו לטבול במקוה זה ולעשות טהרות לכתחילה, ואם טבל ועשה טהרותיו טהורות.

הלכה ג: ספק טומאה צפה על פני המים כיצד, שרץ שהיה צף על פני המים, בין שהיו המים בכלים או בקרקעות וירד למים אפילו אין שם אלא מלא אדם וטומאה ה"ז טהור עד שידע ודאי שנגע, ולא אמרו ספק טומאה צפה טהור אלא לשרץ בלבד, וכל הנתלים והנגררין הרי הן כמונחין.

הלכה ד: שרץ שהיה מונח בכלי וכלי צף על פני המים או שהיה מונח על המת או על הנבילה ואפילו נימוח הנבילה או בשר מת שתחתיו או שהיה מונח על שכבת זרע הצפה על פני המים ה"ז כמונח על הארץ שספקו ברה"י טמא כמו שיתבאר, היה שרץ על גבי שרץ צף על פני המים ה"ז כטומאה עבה שצפה ע"פ המים וספיקו טהור, היה מונח על גבי מי חטאת ומי חטאת צפין על פני המים ה"ז ספק אם הוא כמונח או אינו כמונח, לפיכך יראה לי שספיקו טהור.

הלכה ה: כדרך שטהרו ספק טומאה צפה על פני המים בין בכלים בין בקרקעות כך טהרו ספק טהרה הצפה על פני המים בין בכלים בין בקרקעות, כיצד עריבה שהיא טמא מת וככר תרומה כרוך בסיב או בנייר ונתון בתוכה וירדו לתוכה מי גשמים ונתמלאת ונפשט הנייר והרי הככר צף על פני המים והנייר מבדיל בינו לבין המים וספק נגע צדו בעריבה ספק לא נגע, הרי הוא בטהרתו מפני שהוא צף.

הלכה ו: שרץ שנמצא צף ע"ג בור בתוך הגת לתרומה ספיקו טמא ולפועלין ספיקן טהור, מפני שהיא טומאה צפה.

הלכה ז: ספק משקין לטמא אחרים טהור לטומאת עצמן טמא, כיצד היה מקל בידו ובראשו משקין טמאים וזרקן לתוך ככרות טהורות, ספק נגעו המשקין בככרות ספק לא נגעו טהורות, וכן אם נסתפק לו אם נגעו משקין טמאין בכלי זה או לא נגעו הרי הכלי טהור, וכן אם נסתפק לו אם נגעו משקין אלו הטמאין במשקין אחרים או לא נגעו הרי המשקין האחרים טהורין, אבל טמא שפשט ידו או רגלו לבין משקין טהורין, או שזרק ככר טמא לבין משקין טהורין ספק נגע במשקין ספק לא נגע הרי אלו טמאין בספק וכן כל כיוצא בזה.

הלכה ח: חבית שהיא מליאה משקין ופשט הטמא את ידו לאוירה ספק נגע במשקין ספק לא נגע בהן המשקין טמאין והחבית טהורה שאין ספק המשקין מטמא, וכן אם נכנסו משקין שהן טמאין מספק לאויר החבית הרי החבית טהורה והמשקין שבתוכה טהורין שאינן מתטמאין אלא מן החבית, ואם נתערבו משקין אלו הספק במשקין שבחבית הרי כל המשקין טמאין בספק והחבית טהורה, וכן אם נפלו משקין אלו לתוך התנור הרי הפת והתנור טהורין.

הלכה ט: המרבץ ביתו במים טמאים או שזלפן והיו שם טהרות ספק נתזו עליהן ספק לא נתזו ספיקו טהור.

הלכה י: זלף משקין טהורין וטמאים בתוך הבית ונמצאו אחר כך משקין על ככר של תרומה, נטלה ונשאל עליה הרי זו טהורה שספק המשקין לטמא טהור, הניח הככר עד שינגבו המים שעליה הרי זו טמאה בספק שספק טומאה ברה"י טמא כמו שיתבאר, והרי אין כאן משקין אלא ככר שהיא ספק טמאה ספק טהורה. 

הלכה יא: ספק ידים בין להתטמא בין לטמא אחרים בין לטהרתן טהור כיצד, היו ידיו טהורות ולפניו שני ככרים טמאין ספק נגע ספק לא נגע, או שהיו ידיו טמאות ולפניו שני ככרים טהורים ספק נגע ספק לא נגע, או שהיו ידיו אחת טהורה ואחת טמאה ולפניו שני ככרים טהורים ונגע באחד מהן וספק בטמאה נגע ספק בטהורה, או שהיו ידיו טהורות ולפניו שני ככרים אחד טהור ואחד טמא ונגע באחד מהם ספק בטמא נגע ספק בטהור נגע, או שהיו ידיו אחת טהורה ואחת טמאה ולפניו ככר טמא וככר טהור ונגע בשניהן ספק טמאה בטמא והטהורה בטהור או טמאה בטהור והטהורה בטמא, הידים כמו שהיו והככרים כמות שהיו, וכן אם היו ידיו טמאות והטבילן או נטלן ספק שהמים שטהרו בהן כשרים לידים ספק שהן פסולין, ספק יש בהן כשיעור ספק אין בהן, ספק שהיה דבר חוצץ על ידו ספק שלא היה, הרי ידיו טהורות.

הלכה יב: היתה ידו אחת טמאה ואינו יודע אי זו היא אומרין לו שלא יעשה טהרות עד שיטול שתי ידיו, ואם נגע באחת מהן בטהרות קודם שיטול ידיו טהרותיו טהורות.