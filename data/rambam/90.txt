הלכות ברכות

text

הלכות ברכות. מצות עשה אחת, והיא לברך את השם הגדול והקדוש אחר אכילה. וביאור מצוה זו בפרקים אלו.

פרק א

text

הלכה א: מצות עשה מן התורה לברך אחר אכילת מזון שנאמר ואכלת ושבעת וברכת את יי' אלהיך, ואינו חייב מן התורה אלא אם כן שבע שנאמר ואכלת ושבעת וברכת, ומדברי סופרים אכל אפילו כזית מברך אחריו.

הלכה ב: ומדברי סופרים לברך על כל מאכל תחלה ואח"כ יהנה ממנו, ואפילו נתכוין לאכול או לשתות כל שהוא מברך ואח"כ יהנה, וכן אם הריח ריח טוב מברך ואח"כ יהנה ממנו, וכל הנהנה בלא ברכה מעל, וכן מדברי סופרים לברך אחר כל מה שיאכל וכל מה שישתה, והוא שישתה רביעית והוא שיאכל כזית, ומטעמת אינה צריכה ברכה לא לפניה ולא לאחריה עד רביעית.

הלכה ג: וכשם שמברכין על ההנייה כך מברכין על כל מצוה ומצוה ואח"כ יעשה אותה, וברכות רבות תקנו חכמים דרך שבח והודיה ודרך בקשה כדי לזכור את הבורא תמיד אע"פ שלא נהנה ולא עשה מצוה.

הלכה ד: נמצאו כל הברכות כולן שלשה מינים, ברכות הנייה, וברכות מצות, וברכות הודאה שהן דרך שבח והודיה ובקשה כדי לזכור את הבורא תמיד וליראה ממנו.

הלכה ה: ונוסח כל הברכות עזרא ובית דינו תקנום, ואין ראוי לשנותם ולא להוסיף על אחת מהם ולא לגרוע ממנה, וכל המשנה ממטבע שטבעו חכמים בברכות אינו אלא טועה, וכל ברכה שאין בה הזכרת השם ומלכות אינה ברכה אלא אם כן היתה סמוכה לחבירתה.

הלכה ו: וכל הברכות כולן נאמרין בכל לשון והוא שיאמר כעין שתקנו חכמים, ואם שינה את המטבע הואיל והזכיר אזכרה ומלכות וענין הברכה אפילו בלשון חול יצא.

הלכה ז: כל הברכות כולן צריך שישמיע לאזנו מה שהוא אומר ואם לא השמיע לאזנו יצא בין שהוציא בשפתיו בין שבירך בלבו.

הלכה ח: כל הברכות כולן לא יפסיק בין הברכה ובין הדבר שמברכין עליו בדברים אחרים ואם הפסיק צריך לחזור ולברך שנייה, ואם הפסיק בדברים שהן מענין דברים שמברכין עליו אינו צריך לברך שנייה, כיצד כגון שבירך על הפת וקודם שיאכל אמר הביאו מלח הביאו תבשיל תנו לפלוני לאכול תנו מאכל לבהמה וכיוצא באלו אינו צריך לברך שנית, וכן כל כיוצא בזה.

הלכה ט: כל הברכות כולם מותר לטמא לברך אותן, בין שהיה טמא טומאה שהוא יכול לעלות ממנה בו ביום, בין טומאה שאינו יכול לעלות ממנה בו ביום, ואסור למברך לברך כשהוא ערום עד שיכסה ערותו, במה דברים אמורים באיש אבל באשה יושבת ופניה טוחות בקרקע ומברכת.

הלכה י: כל הברכות כולן אף ע"פ שבירך ויצא ידי חובתו מותר לו לברך לאחרים שלא יצאו ידי חובתן כדי להוציאן, חוץ מברכת ההנייה שאין בה מצוה שאינו מברך לאחרים אלא אם כן נהנה עמהן, אבל ברכת ההנייה שיש בה מצוה כגון אכילת מצה בלילי הפסחים וקידוש היום הרי זה מברך לאחרים ואוכלין ושותים אף על פי שאינו אוכל עמהן.

הלכה יא: כל השומע ברכה מן הברכות מתחלתה ועד סופה ונתכוון לצאת בה ידי חובתו יצא ואע"פ שלא ענה אמן, וכל העונה אמן אחר המברך הרי זה כמברך והוא שיהיה המברך חייב באותה ברכה, היה המברך חייב מדברי סופרים והעונה חייב מן התורה לא יצא ידי חובתו עד שיענה או עד שישמע ממי שהוא חייב בה מן התורה כמוהו.

הלכה יב: רבים שנתועדו לאכול פת או לשתות יין ובירך אחד מהן וענו כולם אמן הרי אלו מותרין לאכול ולשתות, אבל אם לא נתכוונו לאכול כאחד אלא זה בא מעצמו וזה בא מעצמו אע"פ שהן אוכלין מככר אחד כל אחד ואחד מברך לעצמו, במה דברים אמורים בפת ויין בלבד אבל שאר אוכלים ומשקין אינן צריכין הסיבה אלא אם בירך אחד מהן וענו כולן אמן הרי אלו אוכלים ושותין, ואף ע"פ שלא נתכוונו להסב כאחד. 

הלכה יג: כל השומע אחד מישראל מברך ברכה מכל הברכות כולן אע"פ שלא שמע הברכה כולה מתחלתה ועד סופה ואע"פ שאינו חייב באותה ברכה חייב לענות אמן, ואם היה המברך (עכו"ם או) אפיקורוס או כותי או תינוק המתלמד או שהיה גדול ושינה ממטבע הברכה אין עונין אחריהן אמן.

הלכה יד: כל העונה אמן לא יענה לא אמן חטופה ולא אמן קטופה ולא אמן קצרה ולא ארוכה אלא אמן בינונית, ולא יגביה קולו יותר מן המברך, וכל מי שלא שמע את הברכה שהוא חייב בה לא יענה אמן בכלל העונים.

הלכה טו: כל המברך ברכה שאינה צריכה הרי זה נושא שם שמים לשוא והרי הוא כנשבע לשוא ואסור לענות אחריו אמן, התינוקות מלמדין אותן הברכות כתיקונן ואף על פי שהן מברכין לבטלה בשעת לימוד הרי זה מותר ואין עונין אחריהן אמן, והעונה אחריהן אמן לא יצא ידי חובתו.

הלכה טז: כל העונה אמן אחר ברכותיו הרי זה מגונה, והעונה אחר ברכה שהיא סוף ברכות אחרונות הרי זה משובח, כגון אחר בונה ירושלים בברכת המזון ואחר ברכה אחרונה של קריאת שמע של ערבית, וכן בסוף כל ברכה שהיא סוף ברכות אחרונות עונה בה אמן אחר עצמו.

הלכה יז: ולמה יענה אמן אחר בונה ירושלים והרי אחריה ברכת הטוב והמטיב, מפני שברכה זו בימי חכמי משנה תקנוה וכאלו היא תוספת, אבל סוף עיקר הברכות של ברכת המזון היא בונה ירושלים, ולמה לא יענה אמן אחר אהבת עולם מפני שהיא סוף ברכות ראשונות של קריאת שמע וכן כל כיוצא בה מברכות שמברכין אותן תחלה לדבר כגון ברכות שמברכין לפני קריאת מגילה והדלקת נר חנוכה, למען לא יפסיק באמן בין ברכה ובין הדבר שבירך עליו.

הלכה יח: ולמה לא יענה אמן אחר ברכת הפירות וכיוצא בה, מפני שהיא ברכה אחת ואין עונין אמן אלא אחר ברכה אחרונה שקדמה אותה ברכה אחרת או ברכות כגון ברכות המלך וברכות כהן גדול וכיוצא בהן להודיע שכבר השלים כל ברכותיו ולפיכך עונה אמן. 

הלכה יט: כל האוכל דבר האסור בין בזדון בין בשגגה אינו מברך עליו לא בתחלה ולא בסוף, כיצד הרי שאכל טבל של דבריהם או שאכל מעשר ראשון שלא נטלו תרומותיו או מעשר שני והקדש שלא נפדו כהלכתן אינו מברך, ואין צריך לומר אם אכל נבלות וטרפות או שתה יין נסך וכיוצא בו. 

הלכה כ: אבל אם אכל דמאי אע"פ שאינו ראוי אלא לעניים או מעשר ראשון שנטלה תרומתו אע"פ שלא ניטל ממנו חשבון תרומה גדולה והוא שהקדימו בשבלין או מעשר שני והקדש שנפדו אע"פ שלא נתן את החומש, הרי זה מברך תחלה וסוף וכן כל כיוצא בהן.