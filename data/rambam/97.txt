הלכות ברכות

text

פרק ח

text

הלכה א: כל פירות האילן מברכין עליהן בתחלה בורא פרי העץ ולבסוף בורא נפשות רבות חוץ מחמשת המינין הכתובין בתורה, והם ענבים ורמונים ותאנים וזיתים ותמרים שהוא מברך עליהן בסוף ברכה אחת מעין שלש, ועל פירות הארץ והירקות מברכין עליהן בתחלה בורא פרי האדמה ולבסוף בורא נפשות רבות, דברים שאין גידולן מן הארץ כגון בשר וגבינה ודגים וביצים ומים וחלב ודבש וכיוצא בהן בתחלה מברך שהכל ולבסוף בורא נפשות רבות, והשותה מים שלא לרוות צמאו אינו טעון ברכה לא לפניו ולא לאחריו.

הלכה ב: הסוחט פירות והוציא מהן משקין מברך עליהן בתחלה שהכל ולבסוף בורא נפשות, חוץ מן הענבים והזיתים שעל היין הוא מברך בורא פרי הגפן ולבסוף ברכה אחת מעין שלש, ועל השמן בתחלה הוא מברך בורא פרי העץ, במה דברים אמורים שהיה חושש בגרונו ושתה מן השמן עם מי השלקות וכיוצא בהן שהרי נהנה בשתייתו, אבל אם שתה השמן לבדו או שלא היה חושש בגרונו מברך עליו שהכל, שהרי לא נהנה בטעם השמן.

הלכה ג: פירות או ירקות שדרכן להאכל חיים אם בשלן או שלקן מברך עליהן בתחלה שהכל ולבסוף בורא נפשות, וירקות שדרכן להאכל שלוקין כגון כרוב ולפת אם אכלן חיין מברך עליהן בתחלה שהכל ולבסוף בורא נפשות רבות, ואם בשלן או שלקן מברך עליהן בורא פרי האדמה ולבסוף בורא נפשות רבות, דברים שדרכן להאכל חיין ומבושלין אכלן בין חיין בין מבושלין מברך עליהן בתחלה ברכה הראויה להן, אם היו פירות עץ מברך בורא פרי העץ ואם היו פירות האדמה או ירקות מברך בורא פרי האדמה.

הלכה ד: ירקות שדרכן להשלק שלקן מברך על מי שלק שלהן בורא פרי האדמה, והוא ששלקן לשתות מימיהן שמימי השלקות כשלקות במקום שדרכן לשתותן, דבש תמרים מברכין עליו תחלה שהכל, אבל תמרים שמעכן ביד והוציא גרעינין שלהן ועשאן כמו עיסה מברך עליהן תחלה בורא פרי העץ ולבסוף ברכה אחת מעין שלש.

הלכה ה: הקנים המתוקים שסוחטין אותן ומבשלין מימיהן עד שיקפא וידמה למלח, כל הגאונים אומרים שמברכין עליו בורא פרי האדמה, ומקצתם אמרו בורא פרי העץ, וכן אמרו שהמוצץ אותם קנים מברך בורא פרי האדמה, ואני אומר שאין זה פרי ואין מברכין עליו אלא שהכל, שלא יהיה דבש אלו הקנים שנשתנה על ידי אור גדול מדבש תמרים שלא נשתנה על ידי האור ומברכין עליו שהכל.

הלכה ו: הקור והוא ראש הדקל שהוא כמו עץ לבן מברך עליו בתחלה שהכל, קפרס של צלף מברך עליו בורא פרי האדמה מפני שאינו פרי, והאביונות של צלף הן הפרי שהן כצורות תמרים דקים קטנים מברך עליהן בורא פרי העץ.

הלכה ז: הפלפלין והזנגביל בזמן שהן רטובין מברך עליהן בורא פרי האדמה, אבל יבשין אין טעונין ברכה לא לפניהם ולא לאחריהם מפני שהן תבלין ואינו אוכל, וכן אוכלין שאין ראויין לאכילה ומשקין שאינן ראויין לשתייה אינן טעונין ברכה לא לפניהן ולא לאחריהן.

הלכה ח: הפת שעיפשה והיין שהקרים ותבשיל שעברה צורתו והנובלות שהן פגין והשכר והחומץ והגובאי והמלח והכמהין והפטריות על כולן מברך תחלה שהכל, וכל המברכין לפניו שהכל לאחרונה מברך בורא נפשות, וכל הטעון ברכה לאחריו טעון ברכה לפניו.

הלכה ט: שמרים שנתן עליהם שלשה והוציא מהן ארבעה מברך עליהן בורא פרי הגפן שזה יין מזוג הוא, הוציא פחות מארבעה אע"פ שיש בהם טעם יין מברך עליהן שהכל תחלה.

הלכה י: בירך על פירות האילן בורא פרי האדמה יצא, ועל פירות האדמה בורא פרי העץ לא יצא, ועל כולם אם בירך שהכל יצא, ואפי' על הפת ועל היין.

הלכה יא: לקח כוס של שכר בידו והתחיל הברכה על מנת לומר שהכל וטעה ואמר בורא פרי הגפן אין מחזירין אותו, וכן אם היו לפניו פירות הארץ והתחיל הברכה על מנת לומר בורא פרי האדמה וטעה ואמר בורא פרי העץ אין מחזירין אותו, וכן אם היה לפניו תבשיל של דגן ופתח על מנת לומר בורא מיני מזונות וטעה ואמר המוציא יצא, מפני שבשעה שהזכיר את השם והמלכות שהן עיקר הברכה לא נתכוון אלא לברכה הראויה לאותו המין, והואיל ולא היה בעיקר הברכה טעות אע"פ שטעה בסופה יצא ואין מחזירין אותו. 

הלכה יב: כל הברכות האלו אם נסתפק לו בהם אם בירך או לא בירך אינו חוזר ומברך לא בתחלה ולא בסוף, מפני שהן מדברי סופרים, שכח והכניס אוכלין לתוך פיו בלא ברכה, אם היו משקין בולען ומברך עליהן בסוף, ואם היו פירות שאם זרקן ימאסו כגון תותים וענבים מסלקן לצד אחד ומברך ואחר כך בולען, ואם אינן נמאסין כגון פולים ואפונים פולטן מפיו עד שיברך ופיו פנוי ואחר כך אוכל. 

הלכה יג: היו לפניו מינין הרבה אם היו ברכותיהן שוות מברך על אחת מהם ופוטר את השאר, ואם אין ברכותיהם שוות מברך על כל אחת מהן ברכה הראויה לו, ואי זה מהם שירצה להקדים מקדים, ואם אינו רוצה בזה יותר מזה אם יש ביניהם אחד משבעת המינים עליו הוא מברך תחלה, וכל הקודם בפסוק קודם בברכה, והשבעה הן האמורים בפסוק זה ארץ חטה ושעורה וגפן ותאנה ורמון ארץ זית שמן ודבש, ודבש זה הוא דבש תמרים, והתמרים קודמין לענבים, שהתמרים שני לארץ והענבים שלישי לארץ.

הלכה יד: ברכה אחת שהיא מעין שלש של חמשת המינין של פירות ושל יין היא של מיני הדגן, אלא שעל הפירות הוא אומר על העץ ועל פרי העץ ועל תנובת השדה ועל ארץ חמדה וכו', ועל היין הוא אומר על הגפן ועל פרי הגפן, וחותם בשתיהן על הארץ ועל הפירות, ואם היה בארץ ישראל חותם על הארץ ועל פירותיה, ויש מי שמוסיף בברכה שמעין שלש קודם חתימה כי אל טוב ומטיב אתה שהוא מעין ברכה רביעית, ויש מי שאמר שלא תיקנו ברכה רביעית אלא בברכת המזון בלבד.

הלכה טו: שתה יין ואכל תמרים ואכל תבשיל של חמשת מיני דגן מברך באחרונה, ברוך אתה יי' אלהינו מלך העולם על המחיה ועל הכלכלה ועל הגפן ועל פרי הגפן ועל העץ ועל פרי העץ ועל תנובת השדה ועל ארץ חמדה וכו' וחותם ברוך אתה יי' על הארץ ועל המחיה ועל הפירות.

הלכה טז: אבל אם אכל בשר ושתה יין מברך בסוף על זה בפני עצמו ועל זה בפני עצמו, אכל תאנים או ענבים ותפוחים ואגסים וכיוצא בהן מברך בסוף ברכה אחת מעין שלש והיא כוללת הכל מפני שכולן פירות העץ וכן כל כיוצא בזה.