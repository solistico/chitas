הלכות עבודת כוכבים

text

פרק יא

text

הלכה א: אין הולכין בחקות העובדי כוכבים ולא מדמין להן לא במלבוש ולא בשער וכיוצא בהן שנאמר ולא תלכו בחקות הגוים, ונאמר ובחקותיהם לא תלכו, ונאמר השמר לך פן תנקש אחריהם, הכל בענין אחד הוא מזהיר שלא ידמה להן, אלא יהיה הישראל מובדל מהן וידוע במלבושו ובשאר מעשיו כמו שהוא מובדל מהן במדעו ובדעותיו, וכן הוא אומר ואבדיל אתכם מן העמים, לא ילבש במלבוש, המיוחד להן, ולא יגדל ציצית ראשו כמו ציצית ראשם, ולא יגלח מן הצדדין ויניח השער באמצע כמו שהן עושין וזה הנקרא בלורית, ולא יגלח השער מכנגד פניו מאזן לאזן ויניח הפרע מלאחריו כדרך שעושין הן, ולא יבנה מקומות כבנין היכלות של עכו"ם כדי שיכנסו בהן רבים כמו שהן עושין, וכל העושה אחת מאלו וכיוצא בהן לוקה. 

הלכה ב: עכו"ם שהיה מסתפר מישראל כיון שהגיע לבלוריתו קרוב שלש אצבעות לכל רוח שומט את ידו.

הלכה ג: ישראל שהיה קרוב למלכות וצריך לישב לפני מלכיהם והיה לו גנאי לפי שלא ידמה להן הרי זה מותר ללבוש במלבושיהן ולגלח כנגד פניו כדרך שהן עושין.

הלכה ד: אין מנחשין כעכו"ם שנאמר לא תנחשו, כיצד הוא הנחש כגון אלו שאומרים הואיל ונפלה פתי מפי או נפל מקלי מידי איני הולך למקום פלוני היום שאם אלך אין חפציי נעשים, הואיל ועבר שועל מימיני איני יוצא מפתח ביתי היום שאם אצא יפגעני אדם רמאי, וכן אלו ששומעים צפצוף העוף ואומרים יהיה כך ולא יהיה כך, טוב לעשות דבר פלוני ורע לעשות דבר פלוני, וכן אלו שאומרים שחוט תרנגול זה שקרא ערבית, שחוט תרנגולת זו שקראה כמו תרנגול, וכן המשים סימנים לעצמו אם יארע לי כך וכך אעשה דבר פלוני ואם לא יארע לי לא אעשה, כאליעזר עבד אברהם, וכן כל כיוצא בדברים האלו הכל אסור וכל העושה מעשה מפני דבר מדברים אלו לוקה. 

הלכה ה: מי שאמר דירה זו שבניתי סימן טוב היתה עלי, אשה זו שנשאתי ובהמה זו שקניתי מבורכת היתה מעת שקניתיה עשרתי, וכן השואל לתינוק אי זה פסוק אתה לומד אם אמר לו פסוק מן הברכות ישמח ויאמר זה סימן טוב כל אלו וכיוצא בהן מותר הואיל ולא כיון מעשיו ולא נמנע מלעשות אלא עשה זה סימן לעצמו לדבר שכבר היה הרי זה מותר. 

הלכה ו: איזהו קוסם זה העושה מעשה משאר המעשיות כדי שישום ותפנה מחשבתו מכל הדברים עד שיאמר דברים שעתידים להיות ויאמר דבר פלוני עתיד להיות או אינו הווה או שיאמר שראוי לעשות כן והזהרו מכך, יש מן הקוסמין שמשמשים בחול או באבנים, ויש מי שגוהר לארץ וינוע וצועק, ויש מי שמסתכל במראה של ברזל או בעששית ומדמין ואומרים, ויש מי שנושא מקל בידו ונשען עליו ומכה בו עד שתפנה מחשבתו ומדבר, הוא שהנביא אומר עמי בעצו ישאל ומקלו יגיד לו.

הלכה ז: אסור לקסום ולשאול לקוסם אלא שהשואל לקוסם מכין אותו מכת מרדות אבל הקוסם עצמו אם עשה מעשה מכל אלו וכיוצא בהן לוקה שנאמר לא ימצא בך מעביר בנו וגו' קוסם קסמים.

הלכה ח: איזהו מעונן אלו נותני עתים שאומרים באצטגנינות יום פלוני טוב יום פלוני רע יום פלוני ראוי לעשות בו מלאכה פלונית שנה פלונית או חדש פלוני רע לדבר פלוני.

הלכה ט: אסור לעונן אע"פ שלא עשה מעשה אלא הודיע אותן הכזבים שהכסילים מדמין שהן דברי אמת ודברי חכמים, וכל העושה מפני האצטגנינות וכיון מלאכתו או הליכתו באותו העת שקבעו הוברי שמים הרי זה לוקה שנאמר לא תעוננו, וכן האוחז את העינים ומדמה בפני הרואים שעושה מעשה תמהון והוא לא עשה הרי זה בכלל מעונן ולוקה.

הלכה י: איזהו חובר זה שמדבר בדברים שאינן לשון עם ואין להן ענין ומעלה על דעתו בסכלותו שאותן הדברים מועילין, עד שהן אומרים שהאומר כך וכך על הנחש או על העקרב אינו מזיק והאומר כך וכך על האיש אינו ניזוק, ומהן אוחז בידו בעת שמדבר מפתח או סלע וכיוצא בדברים האלו הכל אסור, והחובר עצמו שאחז בידו כלום או שעשה מעשה עם דבורו אפילו הראה באצבעו הרי זה לוקה שנאמר לא ימצא בך וגו' וחובר חבר, אבל אם אמר דברים בלבד ולא הגיד לא אצבע ולא ראש ולא היה בידו כלום, וכן אדם שאמר עליו החבר אותן הקולות והוא יושב לפניו ומדמה שיש לו בזה הנאה מכין אותו מכת מרדות מפני שנשתתף בסכלות החבר, וכל אותן הקולות והשמות המשונים המכוערים לא ירעו וגם היטב אין אותם.

הלכה יא: מי שנשכו עקרב או נחש מותר ללחוש על מקום הנשיכה ואפילו בשבת כדי ליישב דעתו ולחזק לבו, אף על פי שאין הדבר מועיל כלום הואיל ומסוכן הוא התירו לו כדי שלא תטרף דעתו עליו.

הלכה יב: הלוחש על המכה וקורא פסוק מן התורה וכן הקורא על התינוק שלא יבעת והמניח ספר תורה או תפילין על הקטן בשביל שיישן, לא די להם שהם בכלל מנחשים וחוברים אלא שהן בכלל הכופרים בתורה שהן עושין דברי תורה רפואת גוף ואינן אלא רפואת נפשות שנאמר ויהיו חיים לנפשך, אבל הבריא שקרא פסוקין ומזמור מתהילים כדי שתגן עליו זכות קריאתן וינצל מצרות ומנזקים הרי זה מותר.

הלכה יג: איזהו דורש אל המתים זה המרעיב את עצמו והולך ולן בבית הקברות כדי שיבא מת בחלום ויודיעו מה ששאל עליו, ויש אחרים שהם לובשים מלבושים ידועים ואומרים דברים ומקטירין קטרת ידועה וישנים לבדן כדי שיבא מת פלוני ויספר עמו בחלום, כללו של דבר כל העושה כדי שיבא המת ויודיעו לוקה שנאמר לא ימצא בך מעביר וגו' ודורש אל המתים.

הלכה יד: אסור לשאול בעל אוב או בעל ידעוני שנאמר לא ימצא בך מעביר וגו' ושואל אוב וידעוני, נמצאת למד שבעל אוב וידעוני עצמן בסקילה והנשאל בהן באזהרה ומכין אותו מכת מרדות, ואם כיון מעשיו ועשה כפי מאמרן לוקה.

הלכה טו: המכשף חייב סקילה והוא שעשה מעשה כשפים, אבל האוחז את העינים והוא שיראה שעשה והוא לא עשה לוקה מכת מרדות, מפני שלאו זה שנאמר במכשף בכלל לא ימצא בך הוא ולאו שניתן לאזהרת מיתת בית דין הוא ואין לוקין עליו שנאמר מכשפה לא תחיה.

הלכה טז: ודברים האלו כולן דברי שקר וכזב הן והם שהטעו בהן עובדי כוכבים הקדמונים לגויי הארצות כדי שינהגו אחריהן, ואין ראוי לישראל שהם חכמים מחוכמים להמשך בהבלים אלו ולא להעלות על לב שיש תועלת בהן, שנאמר כי לא נחש ביעקב ולא קסם בישראל, ונאמר כי הגוים האלה אשר אתה יורש אותם אל מעוננים ואל קוסמים ישמעו ואתה לא כן וגו', כל המאמין בדברים האלו וכיוצא בהן ומחשב בלבו שהן אמת ודבר חכמה אבל התורה אסרתן אינן אלא מן הסכלים ומחסרי הדעת ובכלל הנשים והקטנים שאין דעתן שלימה, אבל בעלי החכמה ותמימי הדעת ידעו בראיות ברורות שכל אלו הדברים שאסרה תורה אינם דברי חכמה אלא תהו והבל שנמשכו בהן חסרי הדעת ונטשו כל דרכי האמת בגללן, ומפני זה אמרה תורה כשהזהירה על כל אלו ההבלים תמים תהיה עם ה' אלהיך.