הלכות פסולי המוקדשין

text

פרק יב

text

הלכה א: שתי הלחם ולחם הפנים ועומר התנופה שהוסיף במדתן או חסר כל שהוא פסולות.

הלכה ב: חלות תודה ורקיקי נזיר שחסרו עד שלא נזרק דם הזבח פסולין, משנזרק דם הזבח כשירין.

הלכה ג: וכן שתי הלחם שחסרו עד שלא נזרק דמן של כבשים פסולין, משנזרק דמן כשירים.

הלכה ד: וכן שני סדרים שחסרו עד שלא הוקטרו הבזיכין פסולין, משהוקטרו כשירים.

הלכה ה: אבל הנסכים שחסרו, בין משקרב הזבח, בין עד שלא קרב כשרים, ויביא נסכים אחרים למלאותן.

הלכה ו: נסכים שקדשו בכלי שרת ונפסל הזבח, אם נפסל בשחיטה לא קדשו הנסכים ליקרב, נפסל מקבלה ואילך קדשו הנסכים ליקרב, שאין הנסכים מתקדשים ליקרב אלא בשחיטת הזבח, ומה יעשה בהן, אם היה שם זבח אחר זבוח באותה שעה יקרבו עמו, ואם לא היה שם זבח אחר זבוח באותה שעה נעשו כמי שנפסלו בלינה וישרפו, במה דברים אמורים בקרבן צבור מפני שלב בית דין מתנה עליהן, אבל בקרבן יחיד הרי אלו לא יקרבו עם זבח אחר ואף על פי שהוא זבוח באותה שעה אלא מניחן עד שיפסלו בלינה וישרפו. 

הלכה ז: וכל הזבחים שנזבחו שלא לשמן יקרבו נסכיהם.

הלכה ח: ולד תודה ותמורתה, והמפריש תודתו ואבדה והפריש אחרת תחתיה, אם הביאן לאחר שכפר בתודה ראשונה אינן טעונין לחם, ואם עדיין לא כפר בה והרי היא וחליפתה, או היא וולדה, או היא ותמורתה, עומדת, הרי שניהן צריכין לחם, במה דברים אמורים בנודר תודה, אבל תודת נדבה חליפתה ותמורתה טעונין לחם, וולדה אינו טעון לחם, בין לפני כפרה בין לאחר כפרה.

הלכה ט: הפריש תודתו ואבדה, והפריש אחרת תחתיה ואבדה, והפריש אחרת תחתיה ונמצאו הראשונות והרי שלשתן עומדות, נתכפר בראשונה, שנייה אינה טעונה לחם, שלישית טעונה לחם, נתכפר בשלישית, שנייה אינה טעונה לחם, ראשונה טעונה לחם, נתכפר באמצעית שתיהן אינן טעונות לחם.

הלכה י: המפריש מעות לתודתו ואבדו והפריש מעות אחרות תחתיהן ולא הספיק ליקח בהן תודה עד שנמצאו מעות הראשונות, יביא מאלו ומאלו תודה בלחמה, והשאר יביא בהן תודה, ואינה טעונה לחם אבל טעונה נסכים, וכן המפריש תודתו ואבדה והפריש מעות תחתיה ואחר כך נמצאת, יביא במעות תודה בלא לחם, וכן המפריש מעות לתודתו ואבדו והפריש תודה תחתיהן ואחר כך נמצאו המעות, יביא מן המעות תודה ולחמה, וזו התודה האחרונה תקרב בלא לחם.

הלכה יא: האומר הרי זו תודה והרי זה לחמה, אבד הלחם מביא לחם אחר, אבדה התודה אינו מביא תודה אחרת, מפני שהלחם בא בגלל התודה ואין התודה באה בגלל הלחם.

הלכה יב: הפריש מעות לתודתו ונותרו, מביא בהן לחם, הפריש ללחם והותיר אינו מביא בהן תודה.

הלכה יג: האומר הרי זו תודה ונתערבה בתמורתה ומתה אחת מהן ואין ידוע אי זו היא, הרי זו הנשארת אין לה תקנה שאם יביא עמה לחם שמא התמורה היא, ואם הביאה בלא לחם שמא התודה היא, לפיכך לא תקרב זו לעולה אלא תרעה עד שיפול בה מום. 

הלכה יד: תודה שנפרסה חלה מחלותיה כולן פסולות, יצאת החלה או נטמאה, שאר החלות כשירות, נפרס לחמה או נטמא, או יצא עד שלא נשחטה התודה, מביא לחם אחר ושוחט, ואם אחר שנשחט נפרס או נטמא או יצא, הדם יזרק והבשר יאכל, והלחם כולו פסול וידי נדרו לא יצא, נזרק הדם ואחר כך נפרס מקצת הלחם, או נטמא, או יצא, תורם מן השלם על הפרוס, ומן הטהור על הטמא, וממה שבפנים על שבחוץ. 

הלכה טו: תודה שנשחטה על שמונים חלות, לא קדשו ארבעים מתוך שמונים, ואם אמר יקדשו ארבעים מתוך שמונים, מושך ארבעים מתוך שמונים ומרים מהם אחת מכל קרבן, והארבעים השניות יפדו ויצאו לחולין.

הלכה טז: השוחט את התודה והיה לחמה חוץ לחומת בית פגי לא קדש הלחם, אבל אם היה חוץ לעזרה קדש הלחם אע"פ שאינו לפנים.

הלכה יז: שחטה עד שלא קרמו פני הלחם בתנור, ואפילו קרמו כולן חוץ מאחת מהן לא קדש הלחם.

הלכה יח: שחטה ונפסלה בשחיטתה במחשבת זמן או במחשבת מקום קדש הלחם, נמצאת בעלת מום או טריפה או ששחטה שלא לשמה, לא קדש הלחם, וכן הדין באיל נזיר עם הלחם שלו.