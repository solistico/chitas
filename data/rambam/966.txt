הלכות עדות

text

פרק ד

text

הלכה א: עדי נפשות צריכין שיהיו שניהם רואים את העושה עבירה כאחד, וצריכין להעיד כאחד ובבית דין אחד, אבל דיני ממונות אין צריכין לכך, כיצד היה אחד רואהו מחלון זה כשעבר העבירה והעד האחר רואהו מחלון אחר, אם היו שני העדים רואין זה את זה מצטרפין ואם לאו אין מצטרפין, היה זה המתרה בו רואה העדים והעדים רואין אותו אף על פי שאין רואין זה את זה המתרה מצרפן, היו שני העדים בבית אחד והוציא אחד מהן ראשו מן החלון, וראהו זה שעושה מלאכה בשבת ואחד מתרה בו, והכניס ראשו וחזר העד השני והוציא ראשו מאותו החלון וראהו אין מצטרפין עד שיראו שניהם כאחד, היו שני עדים רואין אותו מחלון זה ושני עדים רואין אותו מחלון אחר ואחד מתרה בו באמצע, בזמן שמקצתן רואין אלו את אלו הרי זו עדות אחת ואם לא היו רואין אלו את אלו ולא צירף אותן המתרה הרי אלו שתי עדויות לפיכך אם נמצאת כת אחת מהן זוממין הוא והן נהרגין שהרי הוא נהרג בעדות הכת השניה.

הלכה ב: אבל בדיני ממונות אע"פ שלא ראו אלו את אלו עדותן מצטרפת, כיצד אמר האחד בפני הלוהו ביום פלוני או בפני הודה לו, ואמר העד השני וכן אני מעיד שהלוהו בפני או הודה ביום אחר, הרי אלו מצטרפין.

הלכה ג: וכן אם אמר האחד בפני הלוהו והשני אומר בפני הודה לו, או שאמר הראשון בפני הודה והשני שהעיד אחר זמן אמר בפני הלוהו הרי אלו מצטרפין.

הלכה ד: וכן בעת שמעידין בבית דין יבא אחד ושומעין דבריו היום וכשיבוא העד השני לאחר זמן שומעין דבריו ומצטרפין זה לזה ומוציאין בהן הממון.

הלכה ה: וכן אם היה עד אחד בכתב ואחד על פה מצטרפין, ואם אמר זה שלא כתב עדותו אני קניתי מידו על דבר זה ולא בא המלוה הזה ולא שאל ממני לכתוב שניהם מצטרפין לעשות המלוה בשטר ואינו יכול לומר פרעתי.

הלכה ו: העיד האחד בבית דין זה והעד השני בבית דין אחר יבא בית דין אצל בית דין ויצטרפו עדותן, וכן אם העידו שני העדים בבית דין זה וחזרו והעידו בב"ד אחר יבא אחד מכל בית דין ויצטרפו אבל העד עם הדיין שהעידו שני העדים בפניו אין מצטרפין.

הלכה ז: אע"פ שמצטרפין העדות בדיני ממונות צריך שיעיד כל אחד משניהם בכל דבר כמו שביארנו, אבל אם העיד עד אחד במקצת דבר והעיד השני במקצתו אין מקיימין הדבר מעדות שניהם, שנאמר על פי שנים עדים יקום דבר, כיצד זה אומר פלוני אכל שדה זו שנה פלונית וזה העיד שאכלה שנה שניה וזה העיד שאכלה שנה שלישית, אין מצטרפין עדות שלשתן ואומרים הרי אכלה שלש שנים שכל אחד ואחד העיד במקצת הדבר, וכן אם העיד זה אני ראיתי שערה אחת בצד ימינו של זה וזה אומר אני ראיתי שערה אחת בצד שמאלו של זה באותו היום, אין מצטרפין דברי שניהם כדי שנאמר הרי העידו שניהם שהיה זה גדול ביום פלוני לפי שכל אחד מהם לא העיד אלא במקצת הסימנין, אפילו העידו שנים בשערה אחת והעידו שנים בשערה אחרת שהרי כל כת מהם העידה על חצי דבר ואין זו עדות, אבל אם העידה האחת שראתה שתי שערות בצד ימין והעידה השניה שראתה שתי שערות בצד שמאל מצטרפין וכן כל כיוצא בו.