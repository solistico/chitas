הלכות מקוואות

text

פרק ז

text

הלכה א: אין המקוה נפסל לא בשינוי הטעם ולא בשינוי הריח אלא בשינוי מראה בלבד, וכל דבר שאין עושין בו מקוה לכתחילה פוסל את המקוה בשינוי מראה, כיצד היין או החלב והדם וכיוצא בהן ממי כל הפירות אינן פוסלין את המקוה בג' לוגין שלא אמרו אלא מים שאובין, ופוסלין בשינוי מראה אפילו מקוה שיש בו מאה סאה ונפל לו לוג יין או מי פירות ושינה את מראיו פסול, וכן מקוה שיש בו כ' סאה מים כשרים או פחות מזה ונפל לתוכו סאה יין או מי פירות ולא שינו את מראיו הרי אלו כשירים כשהיו, ואין הסאה שנפלה עולה למדת המקוה, ואם נוסף על הכ' כ' אחרים מים כשירים הרי זה מקוה כשר.

הלכה ב: יש מעלין את המקוה ולא פוסלין פוסלין ולא מעלין לא מעלין ולא פוסלין.

הלכה ג: ואלו מעלין ולא פוסלין: השלג, והברד, והכפור, והגליד, והמלח, וטיט הנרוק, כיצד מקוה שיש בו מ' סאה חסר אחת ונפל לתוכו סאה מא' מאלו ה"ז עולה למדתו והרי המקוה כשר ושלם, נמצאו מעלין ולא פוסלין, אפילו הביא מ' סאה שלג בתחילה והניחן בעוקה וריסקו שם ה"ז מקוה שלם וכשר.

הלכה ד: ואלו פוסלין ולא מעלין: מים שאובין בין טהורין בין טמאים, ומי כבשים, ומי שלקות, והתמד עד שלא החמיץ, והשכר, כיצד מקוה שיש בו מ' סאה חסר משקל דינר ונפל מאחד מאלו משקל דינר לתוכן אינו עולה למדת המקוה ולא השלימו, ואם נפל מאחד מהן שלשה לוגין מהן פוסלין את המקוה.

הלכה ה: ואלו לא פוסלין ולא מעלין: שאר המשקין, ומי פירות, והציר, והמורייס, והתמד משהחמיץ, כיצד מקוה שיש בו מ' סאה חסר אחד ונפל מאחד מאלו סאה לתוכן לא העלהו והרי המים כשרים כשהיו שאין אלו פוסלין אלא בשינוי מראה כמו שביארנו.

הלכה ו: ופעמים שאלו מעלין את המקוה, כיצד מקוה שיש בו מ' סאה ונפל לתוכו סאה מאחד מאלו וחזר ולקח סאה ממנה הרי המ' שנשארו מקוה כשר.

הלכה ז: מקוה שהדיח בו סלי זיתים וענבים ושינו את מראיו כשר.

הלכה ח: מי הצבע פוסלין את המקוה בשלשה לוגין, ואין פוסלין אותו בשינוי מראיו.

הלכה ט: מקוה שנפל יין או מוהל או שאר מי פירות ושינו את מראיו ונפסל כיצד תקנתו ימתין עד שירדו גשמים ויחזרו מראיו למראה מים, ואם היה במקוה מ' סאה מים כשרים ממלא ושואב לתוכן עד שיחזרו מראיו למראה מים, נפל לתוכו יין או מוהל וכיוצא בה ונשתנה מראה מקצתו, אם אין בה מראה מים שלא נשתנה כדי מ' סאה ה"ז לא יטבול בו, והטובל במקום שנשתנה לא עלתה לו טבילה, אפילו חבית של יין שנשברה בים הגדול ומראה אותו מקום כמראה של יין הטובל באותו מקום לא עלתה לו טבילה. 

הלכה י: שלשה לוגין מים שאובין שנפל לתוכן אפילו משקל דינר יין ושינה מראיהן והרי מראה הכל מראה יין ונפלו למקוה לא פסלוהו אלא אם שינו את מראיו.

הלכה יא: שלשה לוגין חסר דינר מים שאובין שנפל לתוכן דינר חלב או מי פירות והרי מראה הכל מים ונפלו למקוה לא פסלוהו עד שיפלו שלשה לוגין מים שאובין שאין בהן תערובת משקה אחר ולא מי פירות.

הלכה יב: מקוה שנשתנה מראה מימיו מחמת עצמו ולא נפל לו דבר ה"ז כשר לא אמרו אלא שנשתנה מחמת משקה אחר.