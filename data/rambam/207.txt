הלכות אישות

text

הלכות אישות. יש בכללן ארבע מצות, שתי מצות עשה, ושתי מצות לא תעשה. וזה הוא פרטן: (א) לישא אשה בכתובה וקידושין. (ב) שלא תבעל אשה בלא כתובה וקידושין. (ג) שלא ימנע שאר כסות ועונה. (ד) לפרות ולרבות ממנה. וביאור מצות אלו בפרקים אלו.

פרק א

text

הלכה א: קודם מתן תורה היה אדם פוגע אשה בשוק אם רצה הוא והיא לישא אותה מכניסה לביתו ובועלה בינו לבין עצמו ותהיה לו לאשה, כיון שנתנה תורה נצטוו ישראל שאם ירצה האיש לישא אשה יקנה אותה תחלה בפני עדים ואחר כך תהיה לו לאשה שנאמר כי יקח איש אשה ובא אליה.

הלכה ב: וליקוחין אלו מצות עשה של תורה הם, ובאחד משלשה דברים האשה נקנית, בכסף או בשטר או בביאה, בביאה ובשטר מן התורה ובכסף מדברי סופרים, וליקוחין אלו הן הנקראין קידושין או אירוסין בכל מקום, ואשה שנקנית באחד משלשה דברים אלו היא הנקראת מקודשת או מאורסת. 

הלכה ג: וכיון שנקנית האשה ונעשית מקודשת אע"פ שלא נבעלה ולא נכנסה לבית בעלה הרי היא אשת איש והבא עליה חוץ מבעלה חייב מיתת בית דין ואם רצה לגרש צריכה גט.

הלכה ד: קודם מתן תורה היה אדם פוגע אשה בשוק אם רצה הוא והיא נותן לה שכרה ובועל אותה על אם הדרך והולך לו, וזו היא הנקראת קדשה, משנתנה התורה נאסרה הקדשה שנאמר לא תהיה קדשה מבנות ישראל, לפיכך כל הבועל אשה לשם זנות בלא קידושין לוקה מן התורה מפני שבעל קדשה. 

הלכה ה: כל שאסר ביאתו בתורה וחייב על ביאתו כרת והן האמורות בפרשת אחרי מות הן הנקראין עריות וכל אחת מהן נקראת ערוה כגון אם ואחות ובת וכיוצא בהן.

הלכה ו: ויש נשים אחרות שאסורות מפי הקבלה ואיסורן מדברי סופרים והן הנקראות שניות מפני שהן שניות לעריות וכל אחת מהן נקראת שנייה, ועשרים נשים הן ואלו הן: א, אם אמו וזו אין לה הפסק אלא אפילו אם אם אם אמו עד מעלה מעלה אסורה, ב, אם אבי אמו בלבד, ג, ואם אביו וזו אין לה הפסק אלא אפילו אם אם אם אביו עד למעלה אסורה, ד, אם אבי אביו בלבד, ה, אשת אבי אביו וזו אין לה הפסק אפילו אשת יעקב אבינו אסורה על אחד ממנו, ו, אשת אבי אמו בלבד, ז, אשת אחי האב מן האם, ח, אשת אחי האם בין מן האם בין מן האב, ט, כלת בנו וזו אין לה הפסק אפילו כלת בן בן בנו עד סוף העולם אסורה עד שתהיה אשת אחד ממנו שנייה על יעקב אבינו, י, כלת בתו בלבד, יא, בת בת בנו בלבד, בת בן בנו בלבד, יג, בת בת בתו בלבד, יד, בת בן בתו בלבד, טו, בת בן בן אשתו בלבד, טז, בת בת בת אשתו בלבד, יז, אם אם אבי אשתו בלבד, יח, אם אב אם אשתו בלבד, יט, אם אם אם אשתו בלבד, כ, אם אב אבי אשתו בלבד, נמצאו השניות שאין להן הפסק ארבע, אם האם עד למעלה, ואם האב עד למעלה, ואשת אבי האב עד למעלה, ואשת בן בנו עד למטה.

הלכה ז: כל שאסר ביאתו בתורה ולא חייב עליו כרת הן הנקראים איסורי לאוין, ועוד נקראין איסורי קדושה, ותשעה הן, ואלו הן, אלמנה לכהן גדול, גרושה, או זונה, או חללה, בין לכהן גדול בין לכהן הדיוט, וממזרת לבן ישראל וכן בת ישראל לממזר, ובת ישראל לעמוני ומואבי, ובת ישראל לפצוע דכא וכרות שפכה, וגרושתו אחר שנשאת לאחר, ויבמה שנשאת לזר ועדיין רשות היבם עליה. החלוצה הרי היא כגרושה והיא אסורה לכהן מדברי סופרים, והנתינים הרי הם כממזרים אחד זכרים ואחד נקבות ואיסורם מדברי סופרים, ובהלכות איסורי ביאה יתבאר לך מה הם הנתינים.

הלכה ח: ויש שאיסור ביאתן בעשה ואינו מחייבי לאוין ושלשה הם, מצרי ואדומי דור ראשון ודור שני אחד זכרים ואחד נקבות, ובעולה לכהן גדול, לפי שלא נאמר באלו לא יבוא או לא יקח אלא מכלל שנאמר דור שלישי יבוא להם בקהל ה' אתה למד שדור ראשון ושני לא יבוא, ומכלל שנאמר והוא אשה בבתוליה יקח אתה למד שאינה בתולה לא יקח, ולאו הבא מכלל עשה הרי הוא כעשה.