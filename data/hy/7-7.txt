ז אדר שני<br />
יום ראשון (תש"ג)<br />
שיעורים: חומש: ויקרא, פרשה ראשונה עם פירש"י.<br />
תהלים: לט-מג.<br />
תניא: פרק לו. והנה 90' …' ואפסי עוד.<br />
<br />
כשהולכים ברחוב צריך להרהר בדברי תורה, במחשבה או בדבור - לפי תנאי המקום אם<br />
הוא מותר, ע"פ דין, באמירת דברי תורה. אך אם מתהלך ואינו עסוק בדברי תורה - מטיחה<br />
בו האבן עליו דורך: בולאך (גולם) מדוע אתה דורך עלי? במה היא עלינותך עלי?!