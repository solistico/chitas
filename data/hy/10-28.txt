כח סיון<br />
יום חמישי (תש"ג)<br />
שיעורים: חומש: קרח, חמישי עם פירש"י.<br />
תהלים: קלה-קלט.<br />
תניא: כי מקור... '168' מדותיו.<br />
<br />
ה"צמח צדק" בספרו לבנו אאזמו"ר [המהר"ש] מאורע מחייו, סיים: בכך שמסייעים<br />
ליהודי בפרנסתו, אפילו כדי להרוויח שבעים "קופיקעס" (מטבע קטנה ברוסיא) בסחר-<br />
עגלים, נפתחים בפניו כל שערי ההיכלות העליונים.<br />
כעבור כמה שנים סיפר אאזמו"ר כל הנ"ל לאאמו"ר [הרש"ב] והוסיף: צריך אפילו לדעת<br />
את הדרך להיכלות העליונים! אך אין זה גורע כלל, העיקר הוא לסייע בלבב שלם וברגש,<br />
שיהיה תענוג בעשיית טובה ליהודי.<br />
