יב סיון<br />
יום שלישי (תשג)<br />
שיעורים: חומש: בהעלותך, שלישי עם פירש"י.<br />
תהלים: סו-סח<br />
תניא: וכדברים... '156' להקדים.<br />
<br />
בברכת "שהכל נהיה בדברו": נהיה - היו"ד בקמץ ולא בסגול.<br />
<br />
אאמו"ר [הרש"ב] כותב באחד ממכתביו: אהוב את הבקורת, כי היא תעמידך על הגובה<br />
האמיתי.<br />
<br />
