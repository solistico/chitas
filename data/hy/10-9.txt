שבת (תש"ג(<br />
שיעורים: חומש: נשא שביעי עם פירש"י.<br />
תהלים: מט-נד.<br />
תניא: וכ"ש... '154' הבריאה.<br />
<br />
העולם זקוק לטיהור האויר. טהרת האויר היא רק ע"י אותיות התורה. האותיות שבתורה<br />
הם שמירה כללית ושמירה פרטית. חלוקת הששה סדרי משנה (בעל פה) היא קיום ה"בלכתך<br />
בדרך". המשנה-משניות שמשננים אותם איפה שנמצאים, בכל מקום שנמצאים - מאירים<br />
את הקשר של ישראל עם קוב"ה. משנה היא אותיות נשמה. קשה למצוא את הביטוי שיבטא<br />
את התועלת הגדולה ביותר, בעזה"י, בשמירה כללית ובשמירה פרטית, שמשיגים, בעזה"י,<br />
ע"י החזרה הסדירה של משניות; וחסרות המילים שיביעו את גודל נחת הרוח, שיגרמו לבורא<br />
עולם ב"ה.<br />
